$(document).ready(function(){
  $(":input").inputmask();
  or
  Inputmask().mask(document.querySelectorAll("input"));
});

$(function(){
	var $btnsWr = $(".js-btns-wr");
	if(!$btnsWr.length) return;

	var $search = $btnsWr.find(".js-search-form"), 
		$phone = $btnsWr.find(".js-phone");
		$reg = $btnsWr.find(".js-reg");
		$searchBtn = $btnsWr.find(".js-btn-search"), 
		$phoneBtn = $btnsWr.find(".js-btn-phone");
		$regBtn = $btnsWr.find(".js-btn-contact");
		$menuBtn = $btnsWr.find(".js-menu-btn"), 
		$menu = $btnsWr.find(".js-menu");
		$menuLink = $btnsWr.find(".js-menu-link");
		$menuClick = $btnsWr.find(".js-click");

	$searchBtn.click(function(){
		$phone.removeClass('show');
		$search.toggleClass('show');
		$menu.removeClass('show');
		$menuBtn.removeClass('close');
	});
	$phoneBtn.click(function(){
		$search.removeClass('show');
		$phone.toggleClass('show');
		$menu.removeClass('show');
		$menuBtn.removeClass('close');
	});
	$regBtn.click(function(){
		$reg.toggleClass('show');
	});

	$menuBtn.click(function(){
		$menu.toggleClass('show');
		$menuBtn.toggleClass('close');
		$phone.removeClass('show');
		$search.removeClass('show');
	});

	$menuLink.click(function(){
		$menuClick.toggleClass('click');
	});

});


$(function () {
	var $Slider = $('.js-slider');
	if(!$Slider.length) return;

	$Slider.slick({
		arrows: true,
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 3,
		responsive: [
		{
		breakpoint: 1240,
		settings: {
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  infinite: true,
		  dots: false
		}
		},
		{
		breakpoint: 768,
		settings: {
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  dots: false
		}
		},
		{
		breakpoint: 480,
		settings: {
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  dots: false
		}
		}
		]
	});
});
